package com.demo.foodtruck.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.demo.foodtruck.dto.FoodTruckDto;
import com.demo.foodtruck.service.inf.LocationService;
import com.google.gson.Gson;

/**
 *
 * @author Sudeep
 */
@SpringBootTest
public class LocationServiceImplTest {

	@Mock
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;

	@MockBean
	private LocationService locationService;

	@BeforeEach
	public void setUp() {

		mockServer = MockRestServiceServer.createServer(restTemplate);

	}


	/**
	 * Test of listFoodTruckLocations method, of class LocationServiceImpl.
	 */
	@Disabled
	@Test
	public void testListFoodTruckLocations() throws Exception {

		Gson gson = new Gson();

		List<FoodTruckDto> foodTruckResult = new ArrayList<>();

		this.mockServer.expect(requestTo("https://data.sfgov.org/resource/rqzj-sfat.json?facilitytype=Truck"))
		.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(gson.toJson(foodTruckResult)));						 

		List<FoodTruckDto> foodTruckDtos = locationService.listFoodTruckLocations(233.3, -131.02);

		mockServer.verify();  	

		assertThat(foodTruckDtos).isEqualTo(foodTruckResult);
	}

}
