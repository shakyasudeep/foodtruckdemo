/**
 * 
 */
package com.demo.foodtruck.service.inf;

import java.util.List;

import com.demo.foodtruck.dto.FoodTruckDto;

/**
 * @author Sudeep
 *
 */
public interface LocationService {

	/**
	 * @param givenLatitude TODO
	 * @param givenLongitude TODO
	 * @return
	 * @throws Exception
	 */
	List<FoodTruckDto> listFoodTruckLocations(Double givenLatitude, Double givenLongitude) throws Exception;

}
