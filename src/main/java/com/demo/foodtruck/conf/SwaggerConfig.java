/**
 * 
 */
package com.demo.foodtruck.conf;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author Sudeep
 *
 */
@Configuration
@Profile("!test")
public class SwaggerConfig {

	@Bean
	public Docket api() {

		return new Docket(DocumentationType.OAS_30)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.demo.foodtruck.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());

	}		

	private ApiInfo apiInfo() {

		return new ApiInfo(
				"Food Truck API", 
				"Description of APIs", 
				"v1", 
				"Terms of Service", 
				new Contact("Sudeep Shakya", "https://www.google.com", "shakyasudeep@live.com"), 
				"License of API", "API license URL", Collections.emptyList());
	}
	
}

