/**
 * 
 */
package com.demo.foodtruck.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

/**
 * @author Sudeep
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"objectid",
"applicant",
"facilitytype",
"cnn",
"locationdescription",
"address",
"blocklot",
"block",
"lot",
"permit",
"status",
"fooditems",
"x",
"y",
"latitude",
"longitude",
"schedule",
"received",
"priorpermit",
"expirationdate",
"location",
":@computed_region_yftq_j783",
":@computed_region_p5aj_wyqh",
":@computed_region_rxqg_mtj9",
":@computed_region_bh8s_q3mv",
":@computed_region_fyvs_ahh9"
})
@Data
public class FoodTruckDto {

	@JsonProperty("objectid")
	public String objectid;

	@JsonProperty("applicant")
	public String applicant;

	@JsonProperty("facilitytype")
	public String facilitytype;

	@JsonProperty("cnn")
	public String cnn;

	@JsonProperty("locationdescription")
	public String locationdescription;

	@JsonProperty("address")
	public String address;

	@JsonProperty("blocklot")
	public String blocklot;

	@JsonProperty("block")
	public String block;

	@JsonProperty("lot")
	public String lot;

	@JsonProperty("permit")
	public String permit;

	@JsonProperty("status")
	public String status;

	@JsonProperty("fooditems")
	public String fooditems;

	@JsonProperty("x")
	public String x;

	@JsonProperty("y")
	public String y;

	@JsonProperty("latitude")
	public Double latitude;

	@JsonProperty("longitude")
	public Double longitude;

	@JsonProperty("schedule")
	public String schedule;

	@JsonProperty("received")
	public String received;

	@JsonProperty("priorpermit")
	public String priorpermit;

	@JsonProperty("expirationdate")
	public String expirationdate;

	@JsonProperty("location")
	public LocationDto location;

	@JsonProperty(":@computed_region_yftq_j783")
	public String computedRegionYftqJ783;

	@JsonProperty(":@computed_region_p5aj_wyqh")
	public String computedRegionP5ajWyqh;

	@JsonProperty(":@computed_region_rxqg_mtj9")
	public String computedRegionRxqgMtj9;

	@JsonProperty(":@computed_region_bh8s_q3mv")
	public String computedRegionBh8sQ3mv;

	@JsonProperty(":@computed_region_fyvs_ahh9")
	public String computedRegionFyvsAhh9;


}
