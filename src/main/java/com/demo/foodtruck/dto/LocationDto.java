/**
 * 
 */
package com.demo.foodtruck.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * @author Sudeep
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"latitude",
	"longitude",
	"human_address"
})
public class LocationDto {

	@JsonProperty("latitude")
	public Double latitude;

	@JsonProperty("longitude")
	public Double longitude;

	@JsonProperty("human_address")
	public String humanAddress;

}