/**
 * 
 */
package com.demo.foodtruck.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

/**
 * @author Sudeep
 *
 */
@Data
public class CustomLoginError {
	
	int status;
	String error;
	Long timestamp;

	/**
	 * @param httpStatus
	 * @param error
	 */
	public CustomLoginError(HttpStatus httpStatus, String error) {
		super();
		this.status = httpStatus.value();
		this.error = error;		
		this.timestamp = System.currentTimeMillis();
	}
	
	

}
