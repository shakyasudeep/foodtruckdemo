/**
 * 
 */
package com.demo.foodtruck.exception;

import java.util.List;
import java.util.stream.Collectors;

import com.demo.foodtruck.util.PropertiesUtil;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sudeep
 *
 */
@Data @NoArgsConstructor
public class ValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1586895690909605927L;

	List<FieldErrorDto> messages;	

	public ValidationException(List<FieldErrorDto> message) {

		this.messages = message.stream()
				.map(m -> new FieldErrorDto(null, m.getField(), PropertiesUtil.getMessageFromErrorCode(m.getMessage())))
				.collect(Collectors.toList());
	}

}
