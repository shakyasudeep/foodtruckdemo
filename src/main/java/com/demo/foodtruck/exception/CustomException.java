package com.demo.foodtruck.exception;

import com.demo.foodtruck.util.PropertiesUtil;

public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public String errorCode;
	public String message;

	public String getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}

	public CustomException(String errorCode, String exceptionCause) {
		super(errorCode);
		this.errorCode=errorCode;
		this.message= PropertiesUtil.getMessage(errorCode,"errorCodes.properties") + ". "+exceptionCause;
	}

	public CustomException(String errorCode, Throwable e) {
		super(errorCode, e);
		this.errorCode=errorCode;
		this.message=e.getLocalizedMessage();
	}

	public CustomException(String errorCode) {
		super(errorCode);
		this.errorCode=errorCode;
		this.message=PropertiesUtil.getMessage(errorCode,"errorCodes.properties");
	}


}
